$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("suruse.feature");
formatter.feature({
  "line": 2,
  "name": "TestCase",
  "description": "",
  "id": "testcase",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@smokeTest"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "To verify user SignUp successful",
  "description": "",
  "id": "testcase;to-verify-user-signup-successful",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "I am open \"chrome\"",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I am open the suruse signIn page URL \"http://dev.suruse.com/\"",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I should see signIn page",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "I click on SignUp Now link",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I should see the SignUp page",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "I enter FullName \"Test \"",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "I enter UserName \"Test\"",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I enter email address",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I enter New Password and Confirm password \"test1234\"",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I click on Agree the terms and policy check box",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I click on SignUp button",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I should see the Thank you page",
  "keyword": "Then "
});
formatter.step({
  "line": 17,
  "name": "I close the browser",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "chrome",
      "offset": 11
    }
  ],
  "location": "TestCase.I_am_open(String)"
});
formatter.result({
  "duration": 5738102868,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://dev.suruse.com/",
      "offset": 38
    }
  ],
  "location": "TestCase.I_am_open_the_suruse_signIn_page_URL(String)"
});
formatter.result({
  "duration": 19714334832,
  "status": "passed"
});
formatter.match({
  "location": "TestCase.I_should_see_signIn_page()"
});
formatter.result({
  "duration": 94117349,
  "status": "passed"
});
formatter.match({
  "location": "TestCase.I_click_on_SignUp_Now_link()"
});
formatter.result({
  "duration": 3106111985,
  "status": "passed"
});
formatter.match({
  "location": "TestCase.I_should_see_the_SignUp_page()"
});
formatter.result({
  "duration": 74401124,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Test ",
      "offset": 18
    }
  ],
  "location": "TestCase.I_enter_FullName(String)"
});
formatter.result({
  "duration": 2133737393,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Test",
      "offset": 18
    }
  ],
  "location": "TestCase.I_enter_UserName(String)"
});
formatter.result({
  "duration": 2102876694,
  "status": "passed"
});
formatter.match({
  "location": "TestCase.I_enter_email_address()"
});
formatter.result({
  "duration": 2230426324,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "test1234",
      "offset": 43
    }
  ],
  "location": "TestCase.I_enter_New_Password_and_Confirm_password(String)"
});
formatter.result({
  "duration": 4222525122,
  "status": "passed"
});
formatter.match({
  "location": "TestCase.I_click_on_Agree_the_terms_and_policy_check_box()"
});
formatter.result({
  "duration": 3106866638,
  "status": "passed"
});
formatter.match({
  "location": "TestCase.I_click_on_SignUp_button()"
});
formatter.result({
  "duration": 3133894197,
  "status": "passed"
});
formatter.match({
  "location": "TestCase.I_should_see_the_Thank_you_page()"
});
formatter.result({
  "duration": 2074202411,
  "status": "passed"
});
formatter.match({
  "location": "TestCase.I_close_the_browser()"
});
formatter.result({
  "duration": 573625942,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 20,
  "name": "To verify user login successful",
  "description": "",
  "id": "testcase;to-verify-user-login-successful",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 21,
  "name": "I am open \"chrome\"",
  "keyword": "Given "
});
formatter.step({
  "line": 22,
  "name": "I am open the suruse signIn page URL \"http://dev.suruse.com/\"",
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "I should see signIn page",
  "keyword": "Then "
});
formatter.step({
  "line": 24,
  "name": "I enter user name \"\u003cuserName\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "I enter Password \"\u003cpassword\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 26,
  "name": "I click on signIn button",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "I should see the home page",
  "keyword": "Then "
});
formatter.step({
  "line": 28,
  "name": "I should see User name on home page",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "I close the browser",
  "keyword": "And "
});
formatter.examples({
  "line": 30,
  "name": "",
  "description": "",
  "id": "testcase;to-verify-user-login-successful;",
  "rows": [
    {
      "cells": [
        "userName",
        "password"
      ],
      "line": 31,
      "id": "testcase;to-verify-user-login-successful;;1"
    },
    {
      "cells": [
        "testautomation@mailinator.com",
        "test123455"
      ],
      "line": 32,
      "id": "testcase;to-verify-user-login-successful;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 32,
  "name": "To verify user login successful",
  "description": "",
  "id": "testcase;to-verify-user-login-successful;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@smokeTest"
    }
  ]
});
formatter.step({
  "line": 21,
  "name": "I am open \"chrome\"",
  "keyword": "Given "
});
formatter.step({
  "line": 22,
  "name": "I am open the suruse signIn page URL \"http://dev.suruse.com/\"",
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "I should see signIn page",
  "keyword": "Then "
});
formatter.step({
  "line": 24,
  "name": "I enter user name \"testautomation@mailinator.com\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "I enter Password \"test123455\"",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 26,
  "name": "I click on signIn button",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "I should see the home page",
  "keyword": "Then "
});
formatter.step({
  "line": 28,
  "name": "I should see User name on home page",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "I close the browser",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "chrome",
      "offset": 11
    }
  ],
  "location": "TestCase.I_am_open(String)"
});
formatter.result({
  "duration": 3988821314,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://dev.suruse.com/",
      "offset": 38
    }
  ],
  "location": "TestCase.I_am_open_the_suruse_signIn_page_URL(String)"
});
formatter.result({
  "duration": 18418091966,
  "status": "passed"
});
formatter.match({
  "location": "TestCase.I_should_see_signIn_page()"
});
formatter.result({
  "duration": 79599882,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "testautomation@mailinator.com",
      "offset": 19
    }
  ],
  "location": "TestCase.I_enter_user_name(String)"
});
formatter.result({
  "duration": 2167518919,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "test123455",
      "offset": 18
    }
  ],
  "location": "TestCase.I_enter_Password(String)"
});
formatter.result({
  "duration": 2108832958,
  "status": "passed"
});
formatter.match({
  "location": "TestCase.I_click_on_signIn_button()"
});
formatter.result({
  "duration": 3118012537,
  "status": "passed"
});
formatter.match({
  "location": "TestCase.I_should_see_the_home_page()"
});
formatter.result({
  "duration": 54140076,
  "error_message": "org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {\"method\":\"xpath\",\"selector\":\".//a[@class\u003d\u0027capital link icon-24 ion-home ng-isolate-scope active\u0027]\"}\n  (Session info: chrome\u003d61.0.3163.100)\n  (Driver info: chromedriver\u003d2.33.506120 (e3e53437346286c0bc2d2dc9aa4915ba81d9023f),platform\u003dWindows NT 6.1.7601 SP1 x86) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 0 milliseconds\nFor documentation on this error, please visit: http://seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.5.3\u0027, revision: \u0027a88d25fe6b\u0027, time: \u00272017-08-29T12:42:44.417Z\u0027\nSystem info: host: \u0027DELL-PC\u0027, ip: \u0027192.168.0.3\u0027, os.name: \u0027Windows 7\u0027, os.arch: \u0027x86\u0027, os.version: \u00276.1\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{mobileEmulationEnabled\u003dfalse, hasTouchScreen\u003dfalse, platform\u003dXP, acceptSslCerts\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, platformName\u003dXP, setWindowRect\u003dtrue, unexpectedAlertBehaviour\u003d, applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.33.506120 (e3e53437346286c0bc2d2dc9aa4915ba81d9023f), userDataDir\u003dC:\\Users\\dell\\AppData\\Local\\Temp\\scoped_dir16344_3893}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, unhandledPromptBehavior\u003d, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, version\u003d61.0.3163.100, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, locationContextEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue}]\nSession ID: 5dc405e6b4704130ff58e66a40ec61d4\n*** Element info: {Using\u003dxpath, value\u003d.//a[@class\u003d\u0027capital link icon-24 ion-home ng-isolate-scope active\u0027]}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:215)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:167)\r\n\tat org.openqa.selenium.remote.http.JsonHttpResponseCodec.reconstructValue(JsonHttpResponseCodec.java:40)\r\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:82)\r\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:45)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:164)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:82)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:646)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:416)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:518)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:361)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:408)\r\n\tat pageobjects.TestPageObject.VerifyElementIsDisplay(TestPageObject.java:78)\r\n\tat pageobjects.TestCase.I_should_see_the_home_page(TestCase.java:158)\r\n\tat ✽.Then I should see the home page(suruse.feature:27)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "TestCase.I_should_see_User_name_on_home_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "TestCase.I_close_the_browser()"
});
formatter.result({
  "status": "skipped"
});
});