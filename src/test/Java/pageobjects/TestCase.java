package pageobjects;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.io.File;
import java.io.IOException;
import java.util.Map;


/**
 * Created by dell on 9/28/2017.
 *
 */
public class TestCase {

    public WebDriver driver = null;
    TestPageObject testPO = new TestPageObject();
    String randomNumber=testPO.generateRandomNumber(4);

   /* @After
    public void tearDown(Scenario scenario) throws IOException {
        if (scenario.isFailed()) {
            // Take a screenshot...
            System.out.println("dfgdfg");
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png"); // ... and embed it in the report.

            File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File("./target/classes/testScreenShot.jpg"));

            driver.close();
        }
    }*/

    @Given("^I am open \"(.*)\"$")
    public void I_am_open(String Browser) throws Throwable {
        if(Browser.equalsIgnoreCase("chrome")){
            ChromeDriverManager.getInstance().setup();
            driver = new ChromeDriver();
            driver.manage().window().maximize();
        }
       /* else if(Browser.equalsIgnoreCase("firefox")){
            FirefoxDriverManager.getInstance().setup();
            driver = new FirefoxDriver();
        }*/
        else if(Browser.equalsIgnoreCase("IE")){
            InternetExplorerDriverManager.getInstance().setup();
            driver = new InternetExplorerDriver();
            driver.manage().window().maximize();
        }
    }

    @When("^I am open the suruse signIn page URL \"(.*)\"$")
    public void I_am_open_the_suruse_signIn_page_URL(String URL) throws Throwable {
        testPO.pause(5);
        testPO.openUnileverHomePage(URL,driver);
    }


    @When("^I click on SignUp Now link$")
    public void I_click_on_SignUp_Now_link () throws Throwable {

        testPO.clickOnElement(testPO.signUpNowLink, driver);
    }


    @Then("^I should see the SignUp page$")
    public void I_should_see_the_SignUp_page() {
        testPO.VerifyElementIsDisplay(driver,testPO.signUpPage);
    }


    @When("^I enter FullName \"(.*)\"$")
    public void I_enter_FullName(String Text) throws Throwable {
        String name=Text+testPO.generateRandomAlphabetic(4);
        testPO.sendkeyOnElement(testPO.fullNameAcc, name, driver);
    }
    @When("^I enter email address$")
    public void I_enter_email_address() throws Throwable {
        String email="test"+randomNumber+"@mailinator.com";
        testPO.sendkeyOnElement(testPO.emailAcc, email, driver);
    }

    @When("^I enter UserName \"(.*)\"$")
    public void I_enter_UserName(String Text) throws Throwable {
        String name=Text+randomNumber;
        testPO.sendkeyOnElement(testPO.userNameAcc, name, driver);
    }

    @When("^I enter New Password and Confirm password \"(.*)\"$")
    public void I_enter_New_Password_and_Confirm_password (String Text) throws Throwable {
        testPO.sendkeyOnElement(testPO.passwordAcc, Text, driver);
        testPO.sendkeyOnElement(testPO.confirmPasswordAcc, Text, driver);
    }
    @When("^I click on Agree the terms and policy check box$")
    public void I_click_on_Agree_the_terms_and_policy_check_box()throws Throwable {
        testPO.clickOnElement(testPO.termsAndPolicyCheckbox, driver);
    }

    @Then("^I should see the Thank you page$")
    public void I_should_see_the_Thank_you_page() {
        testPO.pause(2);
        testPO.VerifyElementIsDisplay(driver,testPO.thankYouScreen);
    }


    @When("^I click on SignUp button$")
    public void I_click_on_SignUp_button()throws Throwable {
        testPO.clickOnElement(testPO.signUpButton, driver);
    }


    @Then("^I should see signIn page$")
    public void I_should_see_signIn_page() {
        testPO.VerifyElementIsDisplay(driver,testPO.signInPage);
    }

    @Then("^I should see signUp link$")
    public void I_should_see_signUp_link() {
        testPO.VerifyElementIsDisplay(driver,testPO.signUpNowLink);
    }

    @When("^I enter user name \"(.*)\"$")
    public void I_enter_user_name (String Text) throws Throwable {
        System.out.println("Enter text : " + Text);
        testPO.sendkeyOnElement(testPO.userName, Text, driver);
    }

    @When("^I enter Password \"(.*)\"$")
    public void I_enter_Password (String Text) throws Throwable {
        System.out.println("Enter Password ");
        testPO.sendkeyOnElement(testPO.password, Text, driver);
    }


    @When("^I click on signIn button$")
    public void I_click_on_signIn_button () throws Throwable {

            testPO.clickOnElement(testPO.signInButton, driver);
    }

    @When("^I should see the home page$")
    public void I_should_see_the_home_page () throws Throwable {

        testPO.VerifyElementIsDisplay(driver,testPO.homePage);
    }

    @When("^I should see User name on home page$")
    public void I_should_see_User_name_on_home_page () throws Throwable {

        testPO.VerifyElementIsDisplay(driver,testPO.homePageUserName);
    }

    @When("^I enter \"(.*)\" in search textbox$")
    public void I_enter_data_in_search_textBox (String Text) throws Throwable {
        System.out.println("Enter Text :"+Text);
        testPO.sendkeyOnElement(testPO.searchTextBox, Text, driver);
        testPO.pressEnterKey(testPO.searchTextBox, driver);
    }
    @When("^I should see the Gmail search results$")
    public void I_should_see_the_Gmail_search_results () throws Throwable {

        testPO.VerifyElementIsDisplay(driver,testPO.searchResult);
    }

    @When("^I click on Premium button$")
    public void I_click_on_Premium_button () throws Throwable {

        testPO.clickOnElement(testPO.premiumButton, driver);
    }

    @When("^I should see the Premium page$")
    public void I_should_see_the_Premium_page () throws Throwable {

        testPO.VerifyElementIsDisplay(driver,testPO.premiumPage);
    }

    @When("^I should see the Premium in breadcrumb$")
    public void I_should_see_the_Premium_in_breadcrumb () throws Throwable {

        testPO.VerifyElementIsDisplay(driver,testPO.premiumInBreadcrumb);
    }
    @When("^I click on New button$")
    public void I_click_on_New_button () throws Throwable {

        testPO.clickOnElement(testPO.newButton, driver);
    }
    @When("^I select the Stream$")
    public void I_select_the_Stream () throws Throwable {

        testPO.clickOnElement(testPO.stream, driver);
    }

    @When("^I should see pop up of create stream$")
    public void I_should_see_pop_up_of_create_stream () throws Throwable {

        testPO.VerifyElementIsDisplay(driver,testPO.createStream);
    }

    @When("^I enter stream name \"(.*)\"$")
    public void I_enter_stream_name (String Text) throws Throwable {
        String StreamName=Text+randomNumber;
        System.out.println("Stream Name :"+StreamName);
        testPO.sendkeyOnElement(testPO.streamName, StreamName, driver);
    }

    @When("^I click on ADD button$")
    public void I_click_on_ADD_button () throws Throwable {
        testPO.clickOnElement(testPO.addButton, driver);
    }

    @When("^I should see a new stream$")
    public void I_should_see_a_new_stream () throws Throwable {
        String displayNewStream = ".//a[@ng-bind='stream.name'][contains(text(),'Test stream"+randomNumber+"')]";
        testPO.pause(5);
        testPO.VerifyElementIsDisplay(driver,displayNewStream);
    }

    @When("^I click on User Profile and select Profile$")
    public void I_click_on_User_Profile_and_select_Profile () throws Throwable {
        testPO.clickOnElement(testPO.userProfilePic, driver);
        testPO.clickOnElement(testPO.profileLink, driver);
    }

    @When("^I should see the user name$")
    public void I_should_see_the_user_name () throws Throwable {
        testPO.pause(5);
        testPO.VerifyElementIsDisplay(driver,testPO.profileUserName);
    }
    @When("^I should see the profile image$")
    public void I_should_see_the_profile_image () throws Throwable {
        testPO.pause(5);
        testPO.VerifyElementIsDisplay(driver,testPO.profileImage);
    }

    @When("^I Click on edit profile button$")
    public void I_click_on_edit_profile_button () throws Throwable {
        testPO.clickOnElement(testPO.editProfileButton, driver);
    }

    @When("^I should see the edit profile page$")
    public void I_should_see_the_edit_profile_page () throws Throwable {
        testPO.VerifyElementIsDisplay(driver,testPO.profileSettengPage);
    }

    @When("^I Change full name of user \"(.*)\"$")
    public void I_Change_full_name_of_user (String Text) throws Throwable {
        testPO.clear(testPO.userNameTextBox, driver);
        String Name=Text+testPO.generateRandomAlphabetic(3);
        testPO.sendkeyOnElement(testPO.userNameTextBox, Name, driver);
    }
    @When("^I click on save button$")
    public void I_click_on_edit_save_button () throws Throwable {
        testPO.clickOnElement(testPO.saveButton, driver);
    }

    @When("^I should see pop up of create group$")
    public void I_should_see_pop_up_of_create_group () throws Throwable {

        testPO.VerifyElementIsDisplay(driver,testPO.createGroup);
        testPO.pause(2);
    }

    @When("^I select the Group$")
    public void I_select_the_Group () throws Throwable {
        testPO.clickOnElement(testPO.group, driver);
    }

    @When("^I should see the Success massage$")
    public void I_should_see_the_Success_massage () throws Throwable {

        testPO.VerifyElementIsDisplay(driver,testPO.profileSuccessMsg);
        testPO.pause(2);
    }

    @When("^I enter group name \"(.*)\"$")
    public void I_enter_group_name (String Text) throws Throwable {
        String groupName=Text+randomNumber;
        System.out.println("Group Name :"+groupName);
        testPO.sendkeyOnElement(testPO.grouName, groupName, driver);
        testPO.pause(2);
    }

    @When("^I click on Groups on left navigation menu$")
    public void I_click_on_Groups_on_left_navigation_menu() throws Throwable {
        testPO.clickOnElement(testPO.groupButton, driver);
        testPO.pause(3);
    }

    @When("^I should see a new Group$")
    public void I_should_see_a_new_Group() throws Throwable {
        String groupName="Test Group"+randomNumber;
        testPO.verifyElementFromList(testPO.groupList,groupName, driver);
        testPO.pause(2);
    }

    @Then("^I close the browser$")
    public void I_close_the_browser() {
            testPO.closeBrowser(driver);
    }
}
