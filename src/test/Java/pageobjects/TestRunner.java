package pageobjects;

/**
 * Created by dell on 9/29/2017.
 */

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import cucumber.api.junit.Cucumber;


@RunWith(Cucumber.class)
/*@CucumberOptions(features = "src/test/resources", glue = "pageobjects")*/
/*
@CucumberOptions( dryRun = false, strict = true, features = "src/test/resources", glue = "pageobjects",
        tags = { "@smokeTest" }, monochrome = true,
        format = { "pretty", "html:target/cucumber", "json:target_json/cucumber.json", "junit:taget_junit/cucumber.xml" } )
*/

@CucumberOptions(plugin = { "html:target/cucumber-html-report",
        "json:target/cucumber.json", "pretty:target/cucumber-pretty.txt",
         "junit:target/cucumber-results.xml" },
        features = { "./src/test/Java/resources" },
        glue = { "pageobjects" })
public class TestRunner {
}

