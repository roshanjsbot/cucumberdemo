package pageobjects;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class TestPageObject {


    /******* pageobjects.Test Locators *******/


    String signInPage = ".//p[contains(text(),'Sign in to your ')]";
    String signUpNowLink = ".//a[contains(text(),'Sign Up Now')]";
    String signUpPage = ".//p[contains(text(),'Creates your ')]/b[contains(text(),'Suruse')]";
    String fullNameAcc = ".//input[@placeholder='Full name']";
    String userNameAcc = ".//input[@placeholder='Username']";
    String emailAcc = ".//input[@placeholder='E-mail']";
    String confirmPasswordAcc = ".//input[@placeholder='Confirm password']";
    String passwordAcc = ".//input[@placeholder='Password']";
    String termsAndPolicyCheckbox = ".//input[@type='checkbox']";
    String signUpButton = ".//input[@value='Sign Up']";
    String thankYouScreen = ".//p[contains(text(),'Thanks for signing up!')]";



    String userName = ".//input[@name='username']";
    String password = ".//input[@type='password']";
    String signInButton = ".//input[@value='Sign In']";
    String homePage = ".//a[@class='capital link icon-24 ion-home ng-isolate-scope active']";
    String homePageUserName = ".//p[@ng-bind='account.username'][contains(text(),'automation123')]";
    String searchTextBox = ".//input[@placeholder='Search susruse.com']";
    String searchResult = ".//p[@ng-bind='media.title'][contains(text(),'Gmail')]";
    String premiumButton = ".//a[contains(text(),'Premium')]";
    String premiumPage = ".//a[@class='capital link icon-24 ion-ribbon-a ng-isolate-scope active']";
    String premiumInBreadcrumb = ".//a[@class='capital link ng-binding ng-isolate-scope active cut'][contains(text(),'Premium')]";
    String newButton = ".//b[contains(text(),'New')]";
    String stream = ".//div[@class='menu links active']/a[contains(text(),'Stream')]";
    String createStream = ".//p[contains(text(),'Create stream')]";
    String streamName = ".//input[@placeholder='Stream Name']";
    String addButton = ".//input[@value='Add']";

    String group = ".//div[@class='menu links active']/a[contains(text(),'Group')]";
    String createGroup = ".//p[contains(text(),'Create group')]";
    String groupButton = ".//div[@class='custom']//a[contains(text(),'Groups')]";
    String groupList = ".//div[@class='details']/a[@ng-bind='group.name']";
    String grouName = ".//input[@placeholder='Group name']";

    String profileSuccessMsg = ".//p[contains(text(),'Profile was successfully updated')]";
    String userNameTextBox = ".//input[@ng-model='user.name']";
    String cancelButton = ".//input[@value='Cancel']";
    String saveButton = ".//input[@value='Save']";
    String editProfileButton = ".//a[contains(text(),'Edit profile')]";
    String userProfileName = ".//p[@ng-bind='profile.name']";
    String profileSettengPage = ".//p[contains(text(),'Profile settings')]";


    String userProfilePic = ".//img[@attr-src='account.profilepic']";
    String profileLink = ".//img[@attr-src='account.profilepic']/../..//a[contains(text(),'Profile')]";
    String profileUserName = ".//a[@ng-bind='profile.username'][contains(text(),'test')]";
    String profileImage = ".//input[@type='file']";



    /******* pageobjects.Test Methods *******/

    public void openUnileverHomePage(String URL,WebDriver driver) {
        driver.get(URL);
        pause(5);
    }

    public void VerifyElementIsDisplay(WebDriver driver,String element) {
        WebElement searchResult = driver.findElement(By.xpath(element));
        searchResult.isDisplayed();

    }

    public void closeBrowser(WebDriver driver) {
        driver.quit();
    }

    public void clickOnElement(String xpath, WebDriver driver) {
        WebElement element = driver.findElement(By.xpath(xpath));
        element.click();
        pause(3);
    }

    public void verifyElementFromList(String element,String text,WebDriver driver) {
        boolean flag = false;
        List<WebElement> list = driver.findElements(By.xpath(element));
        for (WebElement el : list) {
            if (el.getText().equalsIgnoreCase(text)) {
                el.isDisplayed();
                flag=true;
            }

        }
        if (flag==false) {
            Assert.assertTrue(false);
        }
    }

    public void sendkeyOnElement(String xpath, String Text, WebDriver driver) {
        WebElement element = driver.findElement(By.xpath(xpath));
        element.sendKeys(Text);
        pause(2);
    }
    public void pressEnterKey(String xpath, WebDriver driver) {
        WebElement element = driver.findElement(By.xpath(xpath));
        element.sendKeys(Keys.RETURN);
        pause(3);
    }

    public void clear(String xpath, WebDriver driver) {

        driver.findElement(By.xpath(xpath)).clear();
    }

    public String generateRandomAlphabetic(int Length) {
        String RandomName = RandomStringUtils.randomAlphabetic(Length);
        return RandomName;
    }

    public String generateRandomNumber(int Length) {

        String RandomNumber = RandomStringUtils.randomNumeric(Length);
        return RandomNumber;

    }

    public void pause(int secs) {

        try {
            Thread.sleep(secs * 1000);
        } catch (InterruptedException interruptedException) {

        }

    }
}
