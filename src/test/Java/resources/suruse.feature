@smokeTest
Feature: TestCase

	Scenario: To verify user SignUp successful
		Given I am open "chrome"
		When I am open the suruse signIn page URL "http://dev.suruse.com/"
		Then I should see signIn page
		When I click on SignUp Now link
		Then I should see the SignUp page
		When I enter FullName "Test "
		And I enter UserName "Test"
		And I enter email address
		And I enter New Password and Confirm password "test1234"
		And I click on Agree the terms and policy check box
		And I click on SignUp button
		Then I should see the Thank you page
		And I close the browser


	Scenario Outline: To verify user login successful
		Given I am open "chrome"
		When I am open the suruse signIn page URL "http://dev.suruse.com/"
		Then I should see signIn page
		When I enter user name "<userName>"
		And I enter Password "<password>"
		And I click on signIn button
		Then I should see the home page
		And I should see User name on home page
		And I close the browser
		Examples:
			| userName                 |password       |
			| testautomation@mailinator.com     |test123455     |


